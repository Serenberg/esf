package kz.bank.signmessage;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CertFile {

	Document doc = null;
	NodeList nList = null;
	
	public CertFile() throws Exception {
		loadXML();
	}
	
	private void loadXML() throws SAXException, IOException, ParserConfigurationException{
		String path = "C:/fileIn/ESF/certsconf.xml";
		File fXmlFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(fXmlFile);
		nList = doc.getElementsByTagName("item");
	}
	
	private String getTagValue(String tagname, String certificate){
		String value = null;
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String id = eElement.getAttribute("id");
				if(id.equals(certificate)) {
					value = eElement.getElementsByTagName(tagname).item(0).getTextContent();
				}
			}
		}
		return value;
	}
	
	public String getValue(String tagname, String certificate){
		String name = null;
	    name = getTagValue(tagname, certificate); 
		return name;
	}
	
}
