package kz.bank.signmessage;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.AccessController;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PrivilegedExceptionAction;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import javax.security.auth.x500.X500PrivateCredential;
import javax.xml.bind.DatatypeConverter;
import com.ibm.broker.plugin.MbUserException;
import java.security.Signature;
import kz.gov.pki.kalkan.jce.provider.KalkanProvider;

public class KalkanSigner {
	
	public KalkanSigner(){ }

	public PrivateKey privateKey = null;
	
	public String getGostPEM(final String certPath, String certPassword) throws Exception{
		String pem = null;
		X500PrivateCredential x500 = loadX500Credential(certPath, certPassword);
		try {
			X509Certificate cert = x500.getCertificate();
			pem = DatatypeConverter.printBase64Binary(cert.getEncoded());
		} catch (Exception e) {
			throw new MbUserException(this, "getGostPEM()", "", "", e.toString(),
					null);
		}
		return pem;
	}
	
	public String getCertPEM(String certPath, String certPassword) throws Exception{
		String pem = null;
		X509Certificate x509 = loadCredentialsFromResources(certPath,certPassword);
		try {
			pem = DatatypeConverter.printBase64Binary(x509.getEncoded());
		} catch (Exception e) {
			throw new MbUserException(this, "getCertPEM()", "", "", e.toString(),
					null);
		}
		return pem;
	}
	
	public X509Certificate loadCredentialsFromResources(final String certPath, String certPassword) throws Exception{
		X509Certificate x509Certificate = null;
		try {
			KeyStore keystore = KeyStore.getInstance("pkcs12");
			InputStream inputstream;
			inputstream = AccessController.doPrivileged(new PrivilegedExceptionAction<FileInputStream>() {
                @Override
                public FileInputStream run() throws Exception {
                    FileInputStream fis = new FileInputStream(certPath);
                    return fis;
                }
            });
			keystore.load(inputstream, certPassword.toCharArray());
			Enumeration<String> aliases = keystore.aliases();
			String alias = null;
			while(aliases.hasMoreElements()) {
				alias = aliases.nextElement();
			}
			x509Certificate = (X509Certificate) keystore.getCertificate(alias);
		} catch (Exception e) {
			throw new MbUserException(this, "loadCredentialsFromResources()", "", "", e.toString(),
					null);
		}
		return x509Certificate;
	}
	
	public X500PrivateCredential loadX500Credential(final String certPath, String certPassword) throws Exception{
		X500PrivateCredential x500 = null;
		try {
			KalkanProvider kp = new KalkanProvider();
			KeyStore keyStore = KeyStore.getInstance("pkcs12", kp);
			InputStream inputstream;
			inputstream = AccessController.doPrivileged(new PrivilegedExceptionAction<FileInputStream>() {
                @Override
                public FileInputStream run() throws Exception {
                    FileInputStream fis = new FileInputStream(certPath);
                    return fis;
                }
            });
			keyStore.load(inputstream, certPassword.toCharArray());
	        Enumeration<String> e = keyStore.aliases();
	        if(e.hasMoreElements()) {
                String alias = (String)e.nextElement();
                x500 = new X500PrivateCredential((X509Certificate)keyStore.getCertificate(alias), (PrivateKey)keyStore.getKey(alias, certPassword.toCharArray()));
            } else {
                return null;
            }
		} catch(Exception e){
			throw new MbUserException(this, "loadX500Credential()", "", "", e.toString(),
					null);
		}
		return x500;
	}
	
	//���������� xml 
	public String getSignature(String message, final String path, String password) throws Exception{
		String signature = null;
		try {
			X500PrivateCredential x500 = loadX500Credential(path, password);
			PrivateKey pk = x500.getPrivateKey();
			signature = sign(message, pk);
			
		} catch (Exception e) {
			throw new MbUserException(this, "getSignature()", "", "", e.toString(),
					null);
		}
		return signature;
	}
	
	public static String sign(String data, PrivateKey privateKey) throws SignatureException {
		return DatatypeConverter.printBase64Binary(sign(data.getBytes(StandardCharsets.UTF_8), privateKey));
    }

	public static byte[] sign(byte[] data, PrivateKey privateKey) throws SignatureException{
		try {
        	KalkanProvider provider = new KalkanProvider();
        	Signature e = Signature.getInstance(privateKey.getAlgorithm(), provider);
        	e.initSign(privateKey);
            e.update(data);
            return e.sign();
        } catch (InvalidKeyException | NoSuchAlgorithmException var3) {
            throw new RuntimeException(var3);
        }
	}
	
	//��������� alias ����������� �� ����������� ���� � �����������
	public String getAlias(final String path, String password) throws Exception {
		String alias = null;
		try {
			KalkanProvider provider = new KalkanProvider();
			KeyStore keystore = KeyStore.getInstance("pkcs12", provider);
			InputStream inputstream;
			inputstream = AccessController.doPrivileged(new PrivilegedExceptionAction<FileInputStream>() {
                @Override
                public FileInputStream run() throws Exception {
                    FileInputStream fis = new FileInputStream(path);
                    return fis;
                }
            });
			keystore.load(inputstream, password.toCharArray());
			Enumeration<String> aliases = keystore.aliases();
			while(aliases.hasMoreElements()) {
				alias = aliases.nextElement();
			}
		}catch (Exception e){
			throw new MbUserException(this, "getAlias()", "", "", e.toString(),
					null);
		} 
		return alias;
	}
}