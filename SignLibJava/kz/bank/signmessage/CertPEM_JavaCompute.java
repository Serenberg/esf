package kz.bank.signmessage;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.broker.plugin.MbXMLNSC;

public class CertPEM_JavaCompute extends MbJavaComputeNode {

	private static final String ALGORITHM = "AES";
	private static final String KEY = "1Hbfh667adfDEJ78"; 
	
	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal failure = getOutputTerminal("failure");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			
			String message = new String(inMessage.getRootElement().getLastChild().toBitstream(MbXMLNSC.PARSER_NAME, "", "", 546, 1208, 0) , "UTF-8");
			
			MbElement env = inAssembly.getGlobalEnvironment().getRootElement();
			MbElement system = env.getFirstElementByPath("Request/SystemIsQpragma");
			boolean isQpragma = system.getValue().toString() != null;
		
			UserDefinedProperties udp = new UserDefinedProperties(false ,this, message);
			String password = udp.getAuthPassword();
			String alias = udp.getAuth();
			String username = udp.getUsername();
			String userpassword = udp.getUserPassword();
			String tin = udp.getTin();
			
			/*if(isQpragma) {
				userpassword = decrypt(userpassword);
			} */ 
			
			String catalog = this.getUserDefinedAttribute("catalog").toString();
			Alias search = new Alias();
			String getpath = search.getPath(catalog, alias, password);
			
			if (getpath != null) {
				KalkanSigner ks = new KalkanSigner();
				String certificate = ks.getGostPEM(getpath, password);	
				MbElement omroot = outMessage.getRootElement();			
				omroot.createElementAsLastChild(MbElement.TYPE_NAME, "x509Certificate", certificate);
				omroot.createElementAsLastChild(MbElement.TYPE_NAME, "username", username);
				omroot.createElementAsLastChild(MbElement.TYPE_NAME, "userpassword", userpassword);
				omroot.createElementAsLastChild(MbElement.TYPE_NAME, "tin", tin);
			} else {
				failure.propagate(outAssembly);			
			}
		} catch (MbException e) {
			throw e;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		out.propagate(outAssembly);
	}
	
	public static String decrypt(String value) throws Exception {
        Key key = generateKey();
        Cipher cipher = Cipher.getInstance(CertPEM_JavaCompute.ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte [] decryptedValue64 = new BASE64Decoder().decodeBuffer(value);
        byte [] decryptedByteValue = cipher.doFinal(decryptedValue64);
        String decryptedValue = new String(decryptedByteValue,"utf-8");
        return decryptedValue;
                
    }
    
	private static Key generateKey() throws Exception {
	     Key key = new SecretKeySpec(CertPEM_JavaCompute.KEY.getBytes(), CertPEM_JavaCompute.ALGORITHM);
	     return key;
	}
	
}
