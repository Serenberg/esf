package kz.bank.signmessage;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.broker.plugin.MbXMLNSC;

public class Signer_JavaCompute extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal fail = getOutputTerminal("failure");
		
		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
		
			String signedContent = new String(inMessage.getRootElement().getLastChild().getFirstElementByPath("invoice/content").toBitstream(null, "", "", 546, 1208, 0) , "UTF-8");
			String message = new String(inMessage.getRootElement().getLastChild().toBitstream(MbXMLNSC.PARSER_NAME, "", "", 546, 1208, 0) , "UTF-8");
			
			UserDefinedProperties udp = new UserDefinedProperties(false, this, message);
			String certAlias = udp.getGost();
			String password = udp.getGostPassword();
			
			String catalog = this.getUserDefinedAttribute("catalog").toString();
			
			Alias search = new Alias();
			String getpath = search.getPath(catalog, certAlias, password);
			if(getpath != null){
				KalkanSigner ks = new KalkanSigner();
				String signature = ks.getSignature(signedContent, getpath, password);
				String x509 = ks.getGostPEM(getpath, password);
				
				MbElement omroot = outMessage.getRootElement();			
				omroot.createElementAsLastChild(MbElement.TYPE_NAME, "signature", signature);
				omroot.createElementAsLastChild(MbElement.TYPE_NAME, "x509Certificate", x509);
			} else {
				fail.propagate(outAssembly);
			}
			
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}

}
