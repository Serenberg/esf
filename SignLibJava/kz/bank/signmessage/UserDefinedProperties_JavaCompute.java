package kz.bank.signmessage;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.broker.plugin.MbXMLNSC;

public class UserDefinedProperties_JavaCompute extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			
			String message = new String(inMessage.getRootElement().getLastChild().toBitstream(MbXMLNSC.PARSER_NAME, "", "", 546, 1208, 0) , "UTF-8");
			
			UserDefinedProperties udp = new UserDefinedProperties(true, this, message);
			String certificate = udp.getCertificate();
			String username = udp.getUsername();
			String userpassword = udp.getUserPassword();
			String tin = udp.getTin();
			
			CertFile cf = new CertFile();
			String connectcert = cf.getValue("session_cert", certificate);
			String connectpwd = cf.getValue("session_pwd", certificate);
			String signpwd = cf.getValue("sign_pwd", certificate);
			String signcert = cf.getValue("sign_cert", certificate);
			
			String catalog = this.getUserDefinedAttribute("catalog").toString();

			KalkanSigner ks = new KalkanSigner();
			String gostalias = ks.getAlias(catalog + "/" + signcert, connectpwd);
			String authalias = ks.getAlias(catalog + "/" + connectcert, connectpwd);
			
			MbElement omroot = outMessage.getRootElement();			
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "Gost", gostalias);
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "GostPassword", signpwd);
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "Auth", authalias);
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "AuthPassword", signpwd);
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "UserPassword", userpassword);
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "Username", username);
			omroot.createElementAsLastChild(MbElement.TYPE_NAME, "Tin", tin);
			
		} catch (MbException e) {
			throw e;
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		out.propagate(outAssembly);

	}

}
