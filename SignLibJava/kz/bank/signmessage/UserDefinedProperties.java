package kz.bank.signmessage;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbException;

public class UserDefinedProperties {

	private Boolean isTinXpath = false;
	private Boolean isUserPasswordXpath = false;
	private Boolean isUserNameXpath = false;
	private Boolean isCertificateXpath = false;
	private Boolean isGostXpath = false;
	private Boolean isAuthXpath = false;
	private Boolean isGostPasswordXpath = false;
	private Boolean isAuthPasswordXpath = false;
	private Document doc = null;
	
	private MbJavaComputeNode c;
	
	public UserDefinedProperties(boolean userdefinedprop, MbJavaComputeNode c, String message) throws MbException{
		try {
			this.c = c;
			this.isTinXpath = (Boolean) c.getUserDefinedAttribute("isTinXpath");
			this.isUserPasswordXpath = (Boolean) c.getUserDefinedAttribute("isUserPasswordXpath");
			this.isUserNameXpath = (Boolean) c.getUserDefinedAttribute("isUserNameXpath");
			
			if(userdefinedprop){
				this.isCertificateXpath = (Boolean) c.getUserDefinedAttribute("isCertificateXpath");
			} else {
				
				this.isAuthPasswordXpath = (Boolean) c.getUserDefinedAttribute("isAuthPasswordXpath");
				this.isAuthXpath = (Boolean) c.getUserDefinedAttribute("isAuthXpath");
				this.isGostPasswordXpath = (Boolean) c.getUserDefinedAttribute("isGostPasswordXpath");
				this.isGostXpath = (Boolean) c.getUserDefinedAttribute("isGostXpath");
			}
			
			this.doc = convertStringToDocument(message);
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	public String getGostPassword() throws Exception {
		String result = null;
		if(isGostPasswordXpath) {
			String xpath = c.getUserDefinedAttribute("GostPassword").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("GostPassword").toString();
		}
		return result;
	}
	
	public String getGost() throws Exception {
		String result = null;
		if(isGostXpath) {
			String xpath = c.getUserDefinedAttribute("Gost").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("Gost").toString();
		}
		return result;
	}
	
	public String getAuthPassword() throws Exception {
		String result = null;
		if(isAuthPasswordXpath) {
			String xpath = c.getUserDefinedAttribute("AuthPassword").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("AuthPassword").toString();
		}
		return result;
	}
	
	public String getAuth() throws Exception {
		String result = null;
		if(isAuthXpath) {
			String xpath = c.getUserDefinedAttribute("Auth").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("Auth").toString();
		}
		return result;
	}
	
	public String getCertificate() throws Exception {
		String result = null;
		if(isCertificateXpath) {
			String xpath = c.getUserDefinedAttribute("Certificate").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("Certificate").toString();
		}
		return result;
	}

	public String getUsername() throws Exception {
		String result = null;
		if (isUserNameXpath) {
			String xpath = c.getUserDefinedAttribute("UserName").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("UserName").toString();
		}
		return result;
	}
	
	public String getUserPassword() throws Exception {
		String result = null;
		if (isUserPasswordXpath){
			String xpath = c.getUserDefinedAttribute("UserPassword").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("UserPassword").toString();
		}
		return result; 
	}
	
	public String getTin() throws Exception {
		String result = null;
		if (isTinXpath) {
			String xpath = c.getUserDefinedAttribute("Tin").toString();
			result = evaluateXPath(xpath, doc);
		} else {
			result = c.getUserDefinedAttribute("Tin").toString();
		}
		return result;
	}
	
	private String evaluateXPath(String expression, Document doc) throws XPathExpressionException{
		String result = null;
		XPath xPath =  XPathFactory.newInstance().newXPath();
		try {
			Node node = (Node) xPath.evaluate(expression, doc, XPathConstants.NODE);
			result = node.getTextContent();
		} catch (XPathExpressionException e) {
			throw e;
		}
		return result;
	}
	
	private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder;  
        try  {  
            builder = factory.newDocumentBuilder();  
            Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) ); 
            return doc;
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }
}
