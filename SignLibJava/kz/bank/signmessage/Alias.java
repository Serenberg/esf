package kz.bank.signmessage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.ibm.broker.plugin.MbUserException;

public class Alias {

	public Alias(){
		
	}
	
	public String getPath(String path, String alias, String password) throws Exception{
		try {
			List<String> paths = getAllCertsPaths(path);
			for (String p : paths) {
				boolean res = getAlias(p, alias, password);
				if(res){
					return p;
				}
			}
		}catch (Exception e) {
			throw new MbUserException(this, "getPath()", "", "", e.toString(),
					null);
		}
		return null;
	}
	
	public boolean getAlias(String path, String alias, String password) throws Exception{
		try {
			KalkanSigner ks = new KalkanSigner();
			String res;
			res = ks.getAlias(path, password);
			if(res.equals(alias)) {
				return true;
			}
		} catch (Exception e) {
			throw new MbUserException(this, "getAlias()", "", "", e.toString(),
					null);
		}
		return false;
	}
	
	//����� ������ .p12. �� ���� ���������� ���� � ��������
	public List<String> getAllCertsPaths(String path) throws Exception{
		List<String> paths = new ArrayList<String>();
		try {
			
			File[] listOfFiles = new File(path).listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				String abspath = listOfFiles[i].getAbsolutePath();
				if(abspath.toString().toLowerCase().endsWith(".p12")) {
					paths.add(abspath);
				}
			}
		} catch (Exception e) {
			throw new MbUserException(this, "getAllCertsPaths()", "", "", e.toString(),
					null);
		}
		return paths;
	}
	
}
